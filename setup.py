import os
from setuptools import setup

with open('requirements.txt') as f:
    required = f.read().splitlines()

setup(
    name='ip_camera_streamer',
    version='0.1',
    author="Mauricio Martinez Marques",
    include_package_data=True,
    packages=['ip_camera_streamer'],
    platforms=['any'],
    install_requires=required
)
