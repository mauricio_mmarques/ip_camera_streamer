import cv2
from threading import Thread

class WebCamStreamer:

    def __init__(self):
        self.stream = None
        self.frame = None
        self.isStreaming = False

    def startStreaming(self):
        self.isStreaming = True
        self.stream = cv2.VideoCapture(0)
        Thread(target=self.update, args=()).start()

    def update(self):
        while self.isStreaming:
            _, frame = self.stream.read()
            self.frame = frame

    def read(self):
        return self.frame
        
    def stopSteaming(self):
        self.isStreaming = False
