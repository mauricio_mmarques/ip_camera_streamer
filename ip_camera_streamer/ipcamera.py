class IPCamera:

    def __init__(self, url, port, username, password, videoFolder, videoName, videoParams):
        self.url = url
        self.port = port
        self.username = username
        self.password = password
        self.videoFolder = videoFolder
        self.videoName = videoName
        self.videoParams = videoParams

    def cameraUrl(self):
        url = ""
        if self.url is not None:
            url += self.url

        if self.port is None:
            url += ":80"
        else:
            url += ":%d" % (self.port)

        if self.videoFolder is not None:
            url += "/%s" % (self.videoFolder)

        if self.videoName is not None:
            url += "/%s" % (self.videoName)

        if self.videoParams is not None:
            url += "?%s" % (self.videoParams)

        return url
