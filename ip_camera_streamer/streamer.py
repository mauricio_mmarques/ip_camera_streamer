import os
import numpy as np
import cv2
from ip_camera_streamer.ipcamera import IPCamera
import requests
from requests.auth import HTTPBasicAuth
from threading import Thread

class Streamer:

    def __init__(self, camera):
        self.isStreaming = False
        self.camera = camera
        self.bytes = ""
        self.frame = None
        self.r = None

    def startStreaming(self):
        self.isStreaming = True
        self.r = None

        if self.camera.username is not None and self.camera.password is not None:
            self.r = requests.get(self.camera.cameraUrl(), auth=HTTPBasicAuth(self.camera.username, self.camera.password), stream=True)
        else:
            self.r = requests.get(self.camera.cameraUrl(), stream=True)

        self.bytes = ""
        Thread(target=self.update, args=()).start()

    def update(self):
        while self.isStreaming:
        # for chunk in r.iter_content(chunk_size=1024):
        #     if chunk:
            chunk = self.r.raw.read(1024)
            # stream = urllib2.urlopen(self.camera.cameraUrl(), timeout=60)
            self.bytes += chunk#stream.read(1024)
            # 0xff 0xd8 is the starting of the jpeg frame
            a = self.bytes.find('\xff\xd8')
            # 0xff 0xd9 is the end of the jpeg frame
            b = self.bytes.find('\xff\xd9')
            # Taking the jpeg image as byte stream
            if a!=-1 and b!=-1:
                jpg_array = np.fromstring(self.bytes[a:b+2], dtype=np.uint8)
                if len(jpg_array) != 0:
                    jpg = cv2.imdecode(jpg_array,1)
                    self.bytes= self.bytes[b+2:]
                    self.frame = jpg

    def read(self):
        return self.frame

    def stopSteaming(self):
        self.isStreaming = False
